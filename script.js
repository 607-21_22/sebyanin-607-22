async function getHomework() {
    const date = document.getElementById('dateInput').value;
    console.debug(date);
    const response = await fetch(`/homework?date=${date}`);
    const data = await response.json();
    const homeworkList = document.getElementById('homeworkList');
    homeworkList.innerHTML = '';
  
    if (data.length === 0) {
      homeworkList.innerHTML = '<p>Нет заданий на эту дату.</p>';
      return;
    }
  
    const ul = document.createElement('ul');
    data.forEach(item => {
      const li = document.createElement('li');
      li.textContent = `${item.subject}: ${item.assignment}`;
      ul.appendChild(li);
    });
    homeworkList.appendChild(ul);
  }

async function addHomework() {
  const subject = document.getElementById('subjectInput').value;
  const assignment = document.getElementById('assignmentInput').value;
  const date = document.getElementById('newDateInput').value;

  const data = {
    subject: subject,
    assignment: assignment,
    date: date
  };

  const response = await fetch('/add_homework', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });

  if (response.ok) {
    alert('Задание успешно добавлено!');
  } else {
    alert('Произошла ошибка при добавлении задания.');
  }
}