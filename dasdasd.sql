BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "subjects" (
	"id"	INTEGER,
	"name"	TEXT,
	PRIMARY KEY("id")
);
CREATE TABLE IF NOT EXISTS "homework" (
	"id"	INTEGER,
	"subject"	INTEGER,
	"task"	TEXT,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("subject") REFERENCES "subjects"("id")
);
CREATE TABLE IF NOT EXISTS "timetable" (
	"id"	INTEGER,
	"data"	TEXT,
	"task"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("task") REFERENCES "homework"("id")
);
INSERT INTO "subjects" VALUES (1,'Math');
INSERT INTO "subjects" VALUES (2,'History');
INSERT INTO "subjects" VALUES (3,'Philosophy');
INSERT INTO "subjects" VALUES (4,'Physics');
INSERT INTO "subjects" VALUES (5,'Russian');
INSERT INTO "homework" VALUES (1,1,'task 1');
INSERT INTO "homework" VALUES (2,2,'task 2');
INSERT INTO "homework" VALUES (3,3,'task 3');
INSERT INTO "homework" VALUES (4,4,'task 4');
INSERT INTO "homework" VALUES (5,5,'task 5');
INSERT INTO "timetable" VALUES (1,'27.12.2023',1);
INSERT INTO "timetable" VALUES (2,'01.02.2024',2);
INSERT INTO "timetable" VALUES (3,'03.02.2024',3);
INSERT INTO "timetable" VALUES (4,'04.02.2024',4);
INSERT INTO "timetable" VALUES (5,'05.02.2024',5);
COMMIT;
