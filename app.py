from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.responses import FileResponse
from fastapi.staticfiles import StaticFiles
import sqlite3
import os

app = FastAPI()

static_path = os.path.join(os.path.dirname(__file__), "static")

app.mount("/static", StaticFiles(directory=static_path), name="static")

# Подключение к базе данных SQLite
conn = sqlite3.connect('homework.db')
cursor = conn.cursor()

# Создание таблицы для хранения домашнего задания
cursor.execute('''
    CREATE TABLE IF NOT EXISTS homework (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        date TEXT,
        subject TEXT,
        assignment TEXT
    )
''')
conn.commit()

@app.get("/")
async def root() :
    return FileResponse("webapp/index.html")

# Маршрут для получения домашнего задания по заданной дате
@app.get("/homework")
async def get_homework(date: str):
    cursor.execute('SELECT * FROM homework WHERE date = ?', (date,))
    data = cursor.fetchall()
    return [{'subject': row[2], 'assignment': row[3]} for row in data]

class Data(BaseModel):
    subject: str
    assignment: str
    date: str

@app.post("/add_homework")
async def add_homework(data: Data):
    cursor.execute('INSERT INTO homework (date, subject, assignment) VALUES (?, ?, ?)',
                   (data.date, data.subject, data.assignment))
    conn.commit()
    return {"message": "Задание успешно добавлено"}